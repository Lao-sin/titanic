import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    df['Title'] = df['Name'].str.extract(' ([A-Za-z]+)\.', expand=False)

    results = []

    titles_of_interest = ['Mr', 'Mrs', 'Miss']

    for title in titles_of_interest:
        title_group = df[df['Title'] == title]

        median_age = round(title_group['Age'].dropna().median())

        missing_values = title_group['Age'].isnull().sum()

        results.append((title + '.', missing_values, median_age))

        df.loc[(df['Age'].isnull()) & (df['Title'] == title), 'Age'] = median_age
    
    return results
